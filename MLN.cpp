#include<iostream>
#include<time.h>
#include<Windows.h>
using namespace std;

const int MAX_SIZE = 100;

int num1_digits[MAX_SIZE];
int num2_digits[MAX_SIZE];
int res_digits[MAX_SIZE];

void init(int arr[MAX_SIZE]) {
	for (int i = 0; i < MAX_SIZE; i++) {
		arr[i] = 0;
	}
}

void generate(int current_size, int arr[MAX_SIZE]) {
	while (arr[current_size - 1] == 0) {
		arr[current_size - 1] = rand() % 10;
	}
	for (int i = 0; i < current_size; i++) {
		arr[i] = rand() % 10;
	}
}

void MLN(int number1_current_size, int number2_current_size) {

	init(num1_digits);
	init(num2_digits);
	init(res_digits);

	generate(number1_current_size, num1_digits);
	generate(number2_current_size, num2_digits);

	for (int i = 0; i < MAX_SIZE; i++) {
		cout << num1_digits[i];
	}	
	cout << "\n";
	for (int i = 0; i < MAX_SIZE; i++) {
		cout << num2_digits[i];
	}
	cout << "\n";
}

void main() {
	srand(time(NULL));

	for (int number1_current_size = 1; number1_current_size < MAX_SIZE; number1_current_size++) {
		for (int number2_current_size = 1; number2_current_size < MAX_SIZE; number2_current_size++) {
			MLN(number1_current_size, number2_current_size);
		}
	}
}